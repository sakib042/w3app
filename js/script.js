// $(document).ready(function() {
//     $(window).scroll(function() {
//         var scroll = $(window).scrollTop();
//         if (scroll > 600) {
//             $(".bg-faded").css("background", "white");
//             $(".nav-link").css("color", "black");
//             $("#navbarCollapse").css("display", "none");
//         } else {
//             $(".bg-faded").css("background", "transparent");
//             $(".nav-link").css("color", "white");
//         }
//     });

// $(document).ready(function() {
//     $(window).scroll(function() {
//         var scroll = $(window).scrollTop();
//         if (scroll > 600) {
//             $(".nav-link").css("color", "black");

//         } else {
//             $(".nav-link").css("color", "white");
//         }
//     })
// })

var showingSideMenu = false;

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    showingSideMenu = true;
    $('.menu-button').css('display', 'block');
    $('.menu-items').css('display', 'none');
    $('.navbar-brand').css('display', 'none');
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    showingSideMenu = false;
    toggleOnScroll(showingSideMenu);
}
window.onscroll = function() { toggleOnScroll(showingSideMenu) };

function toggleOnScroll(state) {
    if (!state) {
        if (document.documentElement.scrollTop > 100) {
            console.log(document.documentElement.scrollTop);
            $('.menu-button').css('display', 'block');
            $('.menu-items').css('display', 'none');
            $('.navbar-brand').css('display', 'none');

        } else {
            console.log(document.documentElement.scrollTop);
            $('.menu-button').css('display', 'none');
            $('.menu-items').css('display', 'block');
            $('.navbar-brand').css('display', 'block');
        }
    }
}


var a = 0;
$(window).scroll(function() {
    var oTop = $('#counter').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.text()
            }).animate({
                countNum: countTo
            }, {
                duration: 2000,
                easing: 'swing',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                }
            });
        });
        a = 1;
    }
});


$(document).ready(function() {

    $("#myModal").modal("hide");

    // Add smooth scrolling to all links
    $("a.smooth").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });

    $(window).scroll(function() {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function() {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    // Login & registration Toggler

    var toLogin = $("#registrationPanel");
    var regPanel = $("#registrationForm");
    var toReg = $("#loginPanel");
    var loginPanel = $("#loginForm");

    toLogin.click(function(){
        console.log('to login');
        regPanel.css({'display':'none'});
        toLogin.css({'display':'none'});
        toReg.css({'display':'block'});
        loginPanel.css({'display':'block'});
    });
    toReg.click(function(){
        console.log('to reg');
        regPanel.css({'display':'block'});
        toLogin.css({'display':'block'});
        toReg.css({'display':'none'});
        loginPanel.css({'display':'none'});
    });
});